package io.inapinch.generator

import javax.sql.DataSource

interface Generator {
    val request: GeneratorRequest

    fun generate(dataSource: DataSource)
}