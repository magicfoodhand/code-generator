package io.inapinch.generator

import io.inapinch.RunType
import io.inapinch.db.mssql.findProcedures
import io.inapinch.db.mssql.findObjects
import io.inapinch.isOneOf
import javax.sql.DataSource

class GraphqlGenerator(
    override val request: GeneratorRequest
) : Generator {
    override fun generate(dataSource: DataSource) {
        val queries = if(request.type.isOneOf(RunType.GRAPHQL, RunType.GRAPHQL_QUERIES))
            dataSource.findObjects(request.schemas)
        else
            emptyList()

        val mutations = if(request.type.isOneOf(RunType.GRAPHQL, RunType.GRAPHQL_MUTATIONS))
            dataSource.findProcedures(request.schemas)
        else
            emptyList()

    }
}