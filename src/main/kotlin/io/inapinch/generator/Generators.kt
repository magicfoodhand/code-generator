package io.inapinch.generator

import io.inapinch.RunType.*

object Generators {
    fun forRequest(
        request: GeneratorRequest
    ) : Generator = when(request.type) {
        CLASS ->
            ClassGenerator(request)

        RESULT_SET_HANDLER ->
            ResultAdapterGenerator(request)

        REST ->
            RestGenerator(request)

        GRAPHQL,
        GRAPHQL_MUTATIONS,
        GRAPHQL_QUERIES ->
            GraphqlGenerator(request)
    }
}