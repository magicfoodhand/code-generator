package io.inapinch.generator

import io.inapinch.RunType

data class GeneratorRequest(
    val type: RunType,
    val schemas: Set<String>,
    val objectNames: Set<String>,
    val force: Boolean,
    val writeFile: Boolean
) {
    fun generator() = Generators.forRequest(this)
}