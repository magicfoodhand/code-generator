package io.inapinch.shared

import java.sql.ResultSet

fun ResultSet.int(columnName: String) = nullableInt(columnName)!!

fun ResultSet.nullableInt(columnName: String) : Int? {
    val value = this.getInt(columnName)
    return if(value == 0 && this.wasNull())
        null
    else
        value
}