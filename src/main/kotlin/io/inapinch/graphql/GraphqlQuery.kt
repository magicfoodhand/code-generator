package io.inapinch.graphql

typealias Queries = List<GraphqlQuery>

data class GraphqlQuery(val name: String, val input: List<GraphqlInput>, val returnType: List<GraphqlReturnType>)