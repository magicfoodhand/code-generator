package io.inapinch.graphql

data class GraphqlSchema(val queries: Queries, val mutations: Mutations)