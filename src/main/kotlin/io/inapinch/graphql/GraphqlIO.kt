package io.inapinch.graphql

data class GraphqlInput(val name: String)

data class GraphqlReturnType(val name: String)