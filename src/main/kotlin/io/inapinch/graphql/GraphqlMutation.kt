package io.inapinch.graphql

typealias Mutations = List<GraphqlMutation>

data class GraphqlMutation(val name: String, val input: List<GraphqlInput>, val returnType: List<GraphqlReturnType>)