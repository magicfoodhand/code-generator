package io.inapinch

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.options.*
import com.github.ajalt.clikt.parameters.types.*

import io.inapinch.db.createDataSource
import io.inapinch.generator.GeneratorRequest
import kotlin.system.exitProcess

fun <T: Enum<T>> Enum<T>.isOneOf(vararg types: T) = types.contains(this)

enum class RunType {
    CLASS,
    RESULT_SET_HANDLER,
    REST,
    GRAPHQL_QUERIES,
    GRAPHQL_MUTATIONS,
    GRAPHQL;
}

fun main(args: Array<String>) {
    if(System.getenv("DATABASE_URL") == null) {
        System.err.println("DATABASE_URL is not set")
        exitProcess(1)
    }

    CodeGenerator().main(args)
}

class CodeGenerator : CliktCommand() {
    private val databaseUrl by option(envvar = "DATABASE_URL").required()

    private val schemas by option("-s", "--schemas",
        help="The schema names to use for code generation, by default all schemas are included"
    ).multiple()
        .unique()

    private val objectNames by option("-o", "--objects",
        help = "The schema names to use for code generation, by default all objects within schemas are included"
    ).multiple()
        .unique()

    private val type by option("-t", "--type",
        help = "Type of code generation"
    ).enum<RunType> {
        it.name
    }.required()

    private val force by option("-f", "--force",
        help = "Force code generation of Classes and Result Set Handlers for schemas, defaults to false."
    ).flag(default = false)

    private val writeFile by option("-w", "--write-file",
        help = "Create files for all code generation instead of using Standard Out, defaults to false."
    ).flag(default = false)

    override fun run() {
        val generator = GeneratorRequest(type, schemas, objectNames, force, writeFile).generator()

        val dataSource = createDataSource(databaseUrl)
        generator.generate(dataSource)
    }
}