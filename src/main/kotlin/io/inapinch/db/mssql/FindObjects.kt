package io.inapinch.db.mssql

import io.inapinch.db.query
import io.inapinch.graphql.GraphqlQuery
import io.inapinch.graphql.Queries
import io.inapinch.shared.int
import java.sql.ResultSet
import java.util.concurrent.CompletableFuture
import javax.sql.DataSource

private data class SqlServerObject(
    val schema: String,
    val name: String,
    val parameterName: String,
    val parameterId: Int,
    val typeName: String,
    val isTableType: Boolean,
    val maxLength: Int,
    val precision: Int,
    val isNullable: Boolean,
    val parameter: String,
    val columnId: Int,
    val columnName: String
)

private const val OBJECTS_QUERY = """
    ;WITH tvpInfo AS (
        SELECT
            c.name parameter,
            c.column_id,
            c.max_length,
            c.precision,
            c.is_nullable,
            tt.name columnName,
            tvp.user_type_id
        FROM sys.table_types tvp
        LEFT JOIN sys.columns c ON c.object_id = tvp.type_table_object_id
        LEFT JOIN sys.types tt ON tt.system_type_id = c.system_type_id
    )
    SELECT
        SCHEMA_NAME(pp.schema_id) [schema],
        pp.name procedureName,
        p.name parameterName,
        p.parameter_id,
        t.name typeName,
        t.is_table_type,
        IIF(t.is_table_type = 1, tvp.max_length, p.max_length) max_length,
        IIF(t.is_table_type = 1, tvp.precision, p.precision) precision,
        IIF(t.is_table_type = 1, tvp.is_nullable, p.is_nullable) is_nullable,
        tvp.parameter,
        tvp.column_id,
        tvp.columnName
    FROM sys.procedures pp
    JOIN sys.parameters p ON p.object_id = pp.object_id
    JOIN sys.types t ON t.system_type_id = p.system_type_id
    LEFT JOIN tvpInfo tvp ON t.is_table_type = 1 AND tvp.user_type_id = t.user_type_id
    ORDER BY procedureName, parameter_id, column_id
"""

private fun ResultSet.toObject() : SqlServerObject {
    return SqlServerObject(
        this.getString("schema"),
        this.getString("procedureName"),
        this.getString("parameterName"),
        this.int("parameter_id"),
        this.getString("typeName"),
        this.getBoolean("is_table_type"),
        this.int("max_length"),
        this.int("precision"),
        this.getBoolean("is_nullable"),
        this.getString("parameter"),
        this.int("column_id"),
        this.getString("columnName")
    )
}

fun DataSource.findObjects(
    schemas: Set<String>,
    pattern: Regex? = null
) : Queries {
    return this.query(OBJECTS_QUERY, ResultSet::toObject)
        .run {
            if(schemas.isEmpty())
                this
            else
                this.filter { (schema) ->
                    schemas.contains(schema)
                }
        }.run{
            if(pattern == null)
                this
            else
                this.filter { (schema, name) ->
                    pattern.matches("$schema.$name")
                }
        }.groupBy {
            arrayOf(it.schema, it.name)
        }.map { (key, value) ->
            val (schema, name) = key
            CompletableFuture.supplyAsync {
                this.findReturnType("$schema.$name")
            }.thenApply {
                GraphqlQuery(name, emptyList(), emptyList())
            }
        }.map { it.join() }
}