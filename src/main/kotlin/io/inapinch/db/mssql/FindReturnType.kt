package io.inapinch.db.mssql

import io.inapinch.db.query
import java.sql.ResultSet
import javax.sql.DataSource

data class DbStoredProcedureReturnType(val schema: String, val name: String)

fun ResultSet.toReturnType() : DbStoredProcedureReturnType {
    return DbStoredProcedureReturnType(
        this.getString("schema"),
        this.getString("name")
    )
}

fun DataSource.findReturnType(
    procedureName: String
) : List<DbStoredProcedureReturnType> {
    return this.query("EXEC sp_describe_first_result_set ?", ResultSet::toReturnType) {
        it.setNString(1, procedureName)
        it
    }
}