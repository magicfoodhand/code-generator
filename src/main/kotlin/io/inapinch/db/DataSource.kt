package io.inapinch.db

import com.microsoft.sqlserver.jdbc.SQLServerConnectionPoolDataSource
import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import java.sql.PreparedStatement
import java.sql.ResultSet
import javax.sql.DataSource

fun createDataSource(
    connectionUrl: String,
    minConnections: Int = 5,
    maxConnections: Int = 10
) : DataSource {
    val datasource = when {
        connectionUrl.contains("sqlserver") -> SQLServerConnectionPoolDataSource()
        else -> throw IllegalArgumentException("Unsupported type")
    }
    datasource.url = connectionUrl

    val config = HikariConfig()
    config.dataSource = datasource
    config.minimumIdle = minConnections
    config.maximumPoolSize = maxConnections
    return HikariDataSource(config)
}

fun <T> DataSource.query(
    sql: String,
    adapter: (ResultSet) -> T,
    params: (PreparedStatement) -> PreparedStatement = { it }
) : List<T> {
    val results = mutableListOf<T>()
    this.connection.use { connection ->
        connection.prepareStatement(sql).use { ps ->
            val preparedStatement = params(ps)
            preparedStatement.executeQuery().use { resultSet ->
                while(resultSet.next())
                    results.add(adapter(resultSet))
            }
        }
    }
    return results
}